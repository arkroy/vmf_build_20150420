Split Container API Documentation 
Usage
<vmf-split-container> </vmf-split-container>
This directive will create a split pane container area.
For creating a vertical split pane component use the following directive;
<vmf-split-pane-component panewidth="30%" minwidth="30%" maxwidth="50%">
</ vmf-split-pane-component>
For creating a horizontal split pane component use the following directive;
<vmf-split-pane-component paneheight="30%" minwidth="30%" maxwidth="50%">
</ vmf-split-pane-component>

Configuration
panewidth : String
Mandatory: Yes
This attribute will define the default layout width for the vertical split pane component.
paneheight : String
Mandatory: Yes
This attribute will define the default layout height for the horizontal split pane component.
minwidth: String
Mandatory: Yes
This attribute will define the minimum amount of area where the pane component should be dragged.
 maxwidth: String
This attribute will define the maximum amount of area where the pane component should be dragged.

For creating a vertical divider use the following directive;
<vmf-split-pane-divider panewidth="10px" clicks="0" close="right"></vmf-split-pane-divider>
For creating a horizontal divider use the following directive;
<vmf-split-pane-divider paneheight="10px" clicks="0" close="right"></vmf-split-pane-divider>
Configuration
panewidth : String
Mandatory: Yes
This attribute will define the default layout width for the vertical divider.
paneheight : String
Mandatory: Yes
This attribute will define the default layout height for the horizontal divider.
clicks: Integer
Mandatory: Yes
This attribute will initialize the number of clicks on the divider. Initially it will be zero. This attribute is used to implement the double click feature for the directive.
 close: String
Mandatory: Yes
This attribute will define which area to close when double click on vertical/horizontal divider.
Allowed values: right, left, top, bottom






Complete usage example
<vmf-split-container>
<vmf-split-pane-component panewidth="30%" minwidth="30%" maxwidth="50%">
	LEFT PANE
</vmf-split-pane-component>
<vmf-split-pane-divider panewidth="10px" clicks="0" close="right"></vmf-split-pane-divider>
<vmf-split-pane-component><!--RIGHT PANE?
        <vmf-split-container>
              <vmf-split-pane-component paneheight="30%" minwidth="30%" maxwidth="70%">
		TOP PANE
	</vmf-split-pane-component>
	<vmf-split-pane-divider paneheight="10px" clicks="0" close="bottom"></vmf-split-pane-divider>
	<vmf-split-pane-component>
		BOTTOM PANE
	</vmf-split-pane-component>
        </vmf-split-container>
</vmf-split-pane-component>
</vmf-split-container>
